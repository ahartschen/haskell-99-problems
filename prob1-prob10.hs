-- The first 10 problems in the Haskell 99 set.
-- Find them here: https://wiki.haskell.org/99_questions/1_to_10
-- Focus: Lists

-- Problem 1
-- Find the last element of a list.
myLast :: [a] -> a
myLast []     = error "Empty list."
myLast [x]    = x
myLast (_:xs) = myLast xs

-- Problem 2
-- Find the last but one element in a list
myButLast :: [a] -> a
myButLast []     = error "Empty list."
myButLast [x]    = error "Too few elements."
myButLast [x, y] = x
myButLast (_:xs) = myButLast xs

-- Problem 3
-- Find the K'th element in the list

elementAt :: Num a => [b] -> a -> b
elementAt = undefined